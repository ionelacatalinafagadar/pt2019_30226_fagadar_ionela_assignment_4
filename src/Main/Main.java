package Main;
import Presentation.*;
import Business.*;
import Data.RestaurantSerializator;

public class Main {
	public static void main(String[] args) {
		RestaurantSerializator rs = new RestaurantSerializator();
		Restaurant r = new Restaurant();
		r = rs.deserialize();
		View view = new View();
		view.setVisible(true);
		new Controller(rs,r,view);
	}
}
