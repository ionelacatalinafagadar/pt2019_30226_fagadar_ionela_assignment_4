package Presentation;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Business.IRestaurantProcessing;
import Business.Restaurant;

public class AdministratorGraphicalUserInterface extends JFrame implements IRestaurantProcessing{
	private JButton base = new JButton("Base Products");
	private JButton composite = new JButton("Composite Products");
	private JPanel panel;
	private Restaurant r;
	
	public AdministratorGraphicalUserInterface(Restaurant r){
		this.r = r;
		panel = new JPanel();
		panel.add(base);
		panel.add(composite);
		panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));	
		panel.setVisible(true);
        this.setContentPane(panel);
        this.pack();
        this.setTitle("ADMINISTRATOR");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}
	public void addBaseListener(ActionListener baseListener) {
        base.addActionListener(baseListener);
    }
	public void addCompositeListener(ActionListener compositeListener) {
        composite.addActionListener(compositeListener);
    }
}
