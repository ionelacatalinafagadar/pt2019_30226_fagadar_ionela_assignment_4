package Presentation;

import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import Business.IRestaurantProcessing;
import Business.MenuItem;
import Business.Restaurant;

public class BaseGUI extends JFrame implements IRestaurantProcessing{
		MenuItem menuItem;
		Restaurant r;
		
		private JPanel panel;
		private JButton add = new JButton("Add MenuItem");
		private JButton edit = new JButton("Edit MenuItem");
		private JButton delete = new JButton("Delete MenuItem");
		private JButton afis = new JButton("View MenuItems");
		private JLabel lBase= new JLabel("Base Product:");
		
		private JLabel lIdB= new JLabel("Id:");
		private JLabel lNumeB= new JLabel("Nume:");
		private JLabel lPretB= new JLabel("Pret:");
		private JTextField tIdB = new JTextField(11);
		private JTextField tNumeB = new JTextField(25);
		private JTextField tPretB = new JTextField(5);
		
		JTable table;
		
		public BaseGUI(Restaurant r) {
			table = new JTable();
			tIdB.setText("0");
			this.r = r;
			
	        panel = new JPanel();	
	        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));		
	        
	        actualizeazaId();
	        panel.add(lBase);
	        panel.add(lIdB);
	        panel.add(tIdB);
	        panel.add(lNumeB);
	        panel.add(tNumeB);
	        panel.add(lPretB);
	        panel.add(tPretB);
	        
	        panel.add(add);
	        panel.add(edit);
	        panel.add(delete);
	        panel.add(afis);
	        this.setContentPane(panel);
	        
	        this.pack();
	        this.setTitle("Restaurant");
	        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    }

		public void actualizeazaId() {
			tIdB.setText(r.cautaUltimulIdMenu());
		}
		public int getIdB() {
			return Integer.parseInt(tIdB.getText());
		}

		public String getNumeB() {
			return tNumeB.getText();
		}

		public int getPretB() {
			if (!tPretB.getText().equals(""))	return Integer.parseInt(tPretB.getText());
			else return 0;
		}
		public void showError(String errMessage) {
	        JOptionPane.showMessageDialog(this, errMessage);
	    }

		public void addAddMListener(ActionListener addMListener) {
	        add.addActionListener(addMListener);
	    }
		public void addEditMListener(ActionListener editMListener) {
	        edit.addActionListener(editMListener);
	    }
		public void addDeleteMListener(ActionListener deleteMListener) {
	        delete.addActionListener(deleteMListener);
	    }
		public void addAfisListener(ActionListener afisListener) {
	        afis.addActionListener(afisListener);
	    }
}
