package Presentation;

import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import Business.*;

public class ChefGraphicalUserInterface extends JFrame implements Observer {
	ArrayList<Order> comenzi = new ArrayList<Order>();
	JTextArea textArea = new JTextArea();
	JTextArea textArea2 = new JTextArea();
	JPanel panel;
	public ChefGraphicalUserInterface(ArrayList<Order> comenzi){
		this.comenzi = comenzi;
		panel = new JPanel();
		textArea.setEditable(false);
		textArea.setPreferredSize(new Dimension(500,200));
		panel.add(textArea);
		textArea2.setEditable(false);
		textArea2.setPreferredSize(new Dimension(500,200));
		initArea();
		panel.add(textArea2);
		panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));	
		panel.setVisible(true);
        this.setContentPane(panel);
        
        this.pack();
        this.setTitle("BUCATAR");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	}
	public void initArea() {
		textArea.append("COMEZILE BUCATARULUI"+"\n");
		textArea2.append("COMEZILE ACTUALE NEPLATITE"+"\n");
		for (Order ord: comenzi) {
			textArea2.append(ord.toString()+"\n");
		}
	}
	public void setArea() {
		String s = new String();
		s+="COMEZILE ACTUALE NEPLATITE"+"\n";
		for (Order ord: comenzi) {
			s+=ord.toString()+"\n";
		}
		textArea2.setText(s);
	}
	@Override
	public void update(Observable arg0, Object arg1) {
		System.out.println("Se pregateste comanda ");
		textArea.append("Se pregateste " +((Order)arg1).toString()+"\n");
		setArea();
	}
}
