package Presentation;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;

import Business.BaseProduct;
import Business.IRestaurantProcessing;
import Business.MenuItem;
import Business.Restaurant;

public class CompositeGUI extends JFrame implements IRestaurantProcessing{
		MenuItem menuItem;
		Restaurant r;
		
		private JPanel panel;
		private JButton add = new JButton("Add MenuItem");
		private JButton edit = new JButton("Edit MenuItem");
		private JButton delete = new JButton("Delete MenuItem");
		private JButton afis = new JButton("View MenuItems");
		private JLabel lComp= new JLabel("Composite Product:");
		private JLabel lIdC= new JLabel("Id:");
		private JLabel lNumeC= new JLabel("Nume:");
		private JLabel lBaseProducts= new JLabel("Base products:");
		private JTextField tIdC = new JTextField(11);
		private JTextField tNumeC = new JTextField(25);
		private JComboBox<Object> baseProducts;
		
		private JTable table;
		public CompositeGUI(Restaurant r) {
			table = new JTable();
			tIdC.setText("0");
			this.r = r;
			ArrayList <BaseProduct> base = new ArrayList<BaseProduct>();
			for(MenuItem m:r.getMancaruri()) {
				if (m instanceof BaseProduct) base.add((BaseProduct) m);
			}
			
			baseProducts = new JComboBox<Object>(base.toArray());
	        panel = new JPanel();	
	        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));	
	        actualizeazaId();
	        panel.add(lComp);
	        panel.add(lIdC);
	        panel.add(tIdC);
	        panel.add(lNumeC);
	        panel.add(tNumeC);
	        panel.add(lBaseProducts);
	        panel.add(baseProducts);
	        
	        panel.add(add);
	        panel.add(edit);
	        panel.add(delete);
	        panel.add(afis);
	        this.setContentPane(panel);
	        
	        this.pack();
	        this.setTitle("Restaurant");
	        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
	    }
		public void actualizeazaId() {
			tIdC.setText(r.cautaUltimulIdMenu());
		}
		public int getIdC() {
			return Integer.parseInt(tIdC.getText());
		}
		public String getNumeC() {
			return tNumeC.getText();
		}
		public Object getBaseProduct() {
			return baseProducts.getSelectedItem();
		}
		public void showError(String errMessage) {
	        JOptionPane.showMessageDialog(this, errMessage);
	    }

		public void addAddMListener(ActionListener addMListener) {
	        add.addActionListener(addMListener);
	    }
		public void addEditMListener(ActionListener editMListener) {
	        edit.addActionListener(editMListener);
	    }
		public void addDeleteMListener(ActionListener deleteMListener) {
	        delete.addActionListener(deleteMListener);
	    }
		public void addAfisListener(ActionListener afisListener) {
	        afis.addActionListener(afisListener);
	    }
}
