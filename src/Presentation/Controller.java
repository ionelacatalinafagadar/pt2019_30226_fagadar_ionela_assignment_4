package Presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JTable;

import Business.*;
import Data.RestaurantSerializator;
import Presentation.ViewJTable;

public class Controller {
	Restaurant r;
	RestaurantSerializator rs;
	View view;
	AdministratorGraphicalUserInterface viewAdmin;
	WaiterGraphicalUserInterface viewWaiter;
	BaseGUI viewBase;
	CompositeGUI viewComposite;
	ChefGraphicalUserInterface viewChef ;
	
	public Controller(RestaurantSerializator rs,Restaurant r,View view) {
    	this.r = r;
    	viewChef = new ChefGraphicalUserInterface(r.getComenzi());
		r.addObserver(viewChef);
    	this.rs = rs;
		this.view = view;
    	view.addAdminListener(new ViewAdminListener());
    	view.addWaiterListener(new ViewWaiterListener());
    	view.addExitListener(new ViewExitListener());
    }
	class ViewAdminListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			viewAdmin = new AdministratorGraphicalUserInterface(r);
			viewAdmin.addBaseListener(new BaseListener());
			viewAdmin.addCompositeListener(new CompositeListener());
			viewAdmin.setVisible(true);
		}
	}
	class BaseListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			viewBase = new BaseGUI(r);
			viewBase.addAddMListener(new AddMListener());
			viewBase.addEditMListener(new EditMListener());
			viewBase.addDeleteMListener(new DeleteMListener());
			viewBase.addAfisListener(new AfisListener());
			viewBase.setVisible(true);
		}
	}
	class CompositeListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			viewComposite = new CompositeGUI(r);
			viewComposite.addAddMListener(new AddMCListener());
			viewComposite.addEditMListener(new EditMCListener());
			viewComposite.addDeleteMListener(new DeleteMCListener());
			viewComposite.addAfisListener(new AfisListener());
			viewComposite.setVisible(true);
		}
	}
	class ViewWaiterListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			viewWaiter = new WaiterGraphicalUserInterface(r);
			viewWaiter.addAddOListener(new AddOListener());
			viewWaiter.addBillOListener(new BillOListener());
			viewWaiter.addPriceOListener(new PriceOListener());
			viewWaiter.setVisible(true);
			viewChef.setVisible(true);
			viewChef.setLocation(0,270);
		}
	}
	class ViewExitListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			rs.serialize(r);
			view.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
	}
	class AddMListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			boolean introdu = true;
			if (viewBase.getIdB()== 0) viewBase.showError("Id trebuie sa fie diferit de zero!");			
			if (viewBase.getNumeB().equals("") ) {
				viewBase.showError("Introdu toate datele");
				introdu = false;
			}
			BaseProduct b = new BaseProduct(viewBase.getIdB(),viewBase.getNumeB(),viewBase.getPretB());
			if (introdu == true)
				{
				boolean eroare = r.addMenuItem(b);
				if (eroare == true) viewBase.showError("Id nu e unic");
				}
			viewBase.actualizeazaId();
		}
	}
	class EditMListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (viewBase.getIdB()== 0) viewBase.showError("Id trebuie sa fie diferit de zero!");
			BaseProduct b = new BaseProduct(viewBase.getIdB(),viewBase.getNumeB(),viewBase.getPretB());
			boolean eroare = r.editMenuItem(b);
			if (eroare == false) viewBase.showError("Produs negasit");
		}
	}
	class DeleteMListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (viewBase.getIdB()== 0) viewBase.showError("Id trebuie sa fie diferit de zero!");
			int i = r.deleteMenuItem(viewBase.getIdB());
			if (i == -1) viewBase.showError("Produs negasit");
			viewBase.actualizeazaId();
		}
	}
	class AddMCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (viewComposite.getIdC()== 0) viewComposite.showError("Id trebuie sa fie diferit de zero!");
		//	boolean introdu = true;
			if (viewComposite.getNumeC().equals("") ) {
				viewComposite.showError("Introdu toate datele");
			//	introdu = false;
			}
			CompositeProduct c = new CompositeProduct(viewComposite.getIdC(),viewComposite.getNumeC(),(BaseProduct)viewComposite.getBaseProduct());
			r.addMenuItem(c);
		//	viewComposite.actualizeazaId();
		}
	}
	class EditMCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (viewComposite.getIdC()== 0) viewComposite.showError("Id trebuie sa fie diferit de zero!");
			CompositeProduct c = new CompositeProduct(viewComposite.getIdC(),viewComposite.getNumeC(),(BaseProduct)viewComposite.getBaseProduct());
			r.editMenuItem(c);
		}
	}
	class DeleteMCListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			if (viewComposite.getIdC()== 0) viewComposite.showError("Id trebuie sa fie diferit de zero!");
			r.deleteMenuItem(viewComposite.getIdC());
			viewComposite.actualizeazaId();
		}
	}
	public JTable createTable(ArrayList<MenuItem> lista) {
		JTable table;
		String[][] date = new String[100][3];
		String[] coloane = new String[3];
		coloane[0] = "id";
		coloane[1] = "nume";
		coloane[2] = "altele";
			date[0][0] = "id";
			date[0][1] = "nume";
			date[0][2] = "altele";
		int indexColoana = -1;
		for(int i=1;i<lista.size()+1;i++) {
			indexColoana = -1;
			for(Field f:lista.get(i-1).getClass().getDeclaredFields()) {
				if (indexColoana == -1) {
					indexColoana ++;
					continue;
				}
				f.setAccessible(true);
					Object valoare;
					try {
						valoare = f.get(lista.get(i-1));
						date[i][indexColoana] = valoare.toString();
						indexColoana++;
					} catch (IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
			}
		}
		table = new JTable(date,coloane);
		return table;
	}
	class AfisListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			JTable t = createTable(r.getMancaruri());
			ViewJTable viewJTable = new ViewJTable(t);
    		viewJTable.setVisible(true);
		}
	}
	class AddOListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			ArrayList<MenuItem> mancare = new ArrayList<MenuItem>();
			mancare.add((MenuItem) viewWaiter.getProduct());
			r.addComanda(new Order(viewWaiter.getId(),viewWaiter.getDate(), viewWaiter.getMasa()),mancare);
			viewWaiter.schimbaId();
		}
	}
	class BillOListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			r.generateBill(viewWaiter.getMasa());
			viewChef.setArea();
		}
	}
	class PriceOListener implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			int price = r.computePrice(new Order(viewWaiter.getId(),viewWaiter.getDate(), viewWaiter.getMasa()));
			viewWaiter.showError("Total:"+price);
		}
	}
}

