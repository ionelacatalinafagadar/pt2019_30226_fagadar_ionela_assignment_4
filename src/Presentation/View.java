package Presentation;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class View extends JFrame {
	private JButton admin = new JButton("Administrator");
	private JButton waiter = new JButton("Waiter");
	private JButton exit = new JButton("Exit");
	private JPanel panel;
	
	public View(){
		panel = new JPanel();
		panel.add(admin);
		panel.add(waiter);
		panel.add(exit);
		panel.setLayout(new BoxLayout(panel,BoxLayout.X_AXIS));	
		panel.setVisible(true);
        this.setContentPane(panel);
        
        this.pack();
        this.setTitle("Restaurant");
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	}
	public void addAdminListener(ActionListener adminListener) {
        admin.addActionListener(adminListener);
    }
	public void addWaiterListener(ActionListener waiterListener) {
        waiter.addActionListener(waiterListener);
    }
	public void addExitListener(ActionListener exitListener) {
        exit.addActionListener(exitListener);
    }
}
