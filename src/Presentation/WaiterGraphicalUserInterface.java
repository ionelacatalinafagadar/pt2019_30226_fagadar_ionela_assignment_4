package Presentation;

import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Business.BaseProduct;
import Business.IRestaurantProcessing;
import Business.MenuItem;
import Business.Restaurant;

public class WaiterGraphicalUserInterface extends JFrame implements IRestaurantProcessing {
	MenuItem menuItem;
	Restaurant r;
	private JPanel panel;
	private JButton add = new JButton("Add Order");
	private JButton price = new JButton("Compute Price");
	private JButton bill = new JButton("Generate Bill");
	private JLabel lIdO= new JLabel("Id:");
	private JLabel lDateO= new JLabel("Data:");
	private JLabel lTableO= new JLabel("Masa:");
	private JLabel lProductO= new JLabel("Produs:");
	private JTextField tIdO = new JTextField(11);
	private JTextField tDateO = new JTextField(25);
	private JTextField tTableO = new JTextField(2);
	private JComboBox<Object> products;
	
	public WaiterGraphicalUserInterface(Restaurant r) {
		tIdO.setText("0");
		this.r = r;
		ArrayList <BaseProduct> base = new ArrayList<BaseProduct>();
		
		products = new JComboBox<Object>(r.getMancaruri().toArray());
        panel = new JPanel();	
        panel.setLayout(new BoxLayout(panel,BoxLayout.Y_AXIS));
        Date currentDate = new Date();
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        tDateO.setText(format.format(currentDate));
        schimbaId();
        panel.add(lIdO);
        panel.add(tIdO);
        panel.add(lDateO);
        panel.add(tDateO);
        panel.add(lTableO);
        panel.add(tTableO);
        
        panel.add(lProductO);
        panel.add(products);
        
        panel.add(add);
        panel.add(price);
        panel.add(bill);
        this.setContentPane(panel);
        
        this.pack();
        this.setTitle("CHELNER");
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
    }
	public int getId() {
		return Integer.parseInt(tIdO.getText());
	}
	public int getMasa() {
		return Integer.parseInt(tTableO.getText());
	}
	public String getDate() {
		return tDateO.getText();
	}
	public Object getProduct() {
		return products.getSelectedItem();
	}
	public void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
    }
	public void schimbaId() {
		 tIdO.setText(r.cautaUltimulId());
		 System.out.println(r.cautaUltimulId());
	}
	public void addAddOListener(ActionListener addMListener) {
        add.addActionListener(addMListener);
    }
	public void addPriceOListener(ActionListener priceListener) {
        price.addActionListener(priceListener);
    }
	public void addBillOListener(ActionListener billListener) {
        bill.addActionListener(billListener);
    }
}
